package org.home.sleepcyclecalculator

/**
 * Created by criskey on 27/6/2016.
 */
class SleepCycleCalculator(val wakeUpTime: Time) {

    val startSleepingTimes: Array<Time?>
        get() {
            val fallAsleepPoints = NO_OF_SLEEP_CYCLES - 2
            val times = arrayOfNulls<Time>(fallAsleepPoints)
            val startingTime = wakeUpTime.wakeUpTimeInMinutes - TOTAL_MINUTES_TO_SLEEP + MINUTES_IN_A_DAY
            for (i in 0..fallAsleepPoints - 1) {
                val timeInMinutes:Int = startingTime + MINUTES_IN_A_SLEEP_CYCLE * i
                val hour = (timeInMinutes / 60.0).toInt() % 24
                val minutes = timeInMinutes % 60
                times[i] = Time(hour, minutes);
            }
            return times
        }

    companion object {

        private val AM = "AM";

        private val PM = "PM"

        private val MINUTES_IN_A_DAY = 1440// 24 * 60

        private val MINUTES_IN_A_SLEEP_CYCLE = 90 // minutes

        private val NO_OF_SLEEP_CYCLES = 6// cycles per night

        private val TOTAL_MINUTES_TO_SLEEP = MINUTES_IN_A_SLEEP_CYCLE * NO_OF_SLEEP_CYCLES
    }
}