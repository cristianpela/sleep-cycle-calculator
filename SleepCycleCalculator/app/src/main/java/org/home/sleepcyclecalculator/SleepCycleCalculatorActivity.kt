package org.home.sleepcyclecalculator

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.Window
import kotlinx.android.synthetic.main.fragment_sleep_cycle_set.*

class SleepCycleCalculatorActivity : AppCompatActivity() {

    var mProcessedFragment: SleepCycleProcessedFragment? = null;
    var mSetFragment: SleepCycleSetFragment? = null;


    override fun onCreate(savedInstanceState: Bundle?) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sleep_cycle_calculator);
        initFragments(savedInstanceState);

    }

    fun actionProcess(button: View) {
        addProcessedFragment()
    }

    fun actionSavedAlarm(button: View) {
        addProcessedFragment(true);
    }

    fun actionSaveAlarm(button: View) {
        mProcessedFragment?.setSaveTime()
    }

    fun actionCancelAlarm(button : View){
        AlarmSetup(this).cancel()
        button.visibility = View.GONE
    }

    fun initFragments(savedInstanceState: Bundle?) {
        if (savedInstanceState == null) {
            mSetFragment = SleepCycleSetFragment.newInstance();
            supportFragmentManager.beginTransaction()
                    .add(R.id.container, mSetFragment, SleepCycleSetFragment.TAG)
                    .commit();
        } else {
            mSetFragment = supportFragmentManager.findFragmentByTag(SleepCycleSetFragment.TAG) as SleepCycleSetFragment;
        }
    }

    fun initProcessedFragment() {
        if (mProcessedFragment == null) {
            mProcessedFragment = supportFragmentManager.findFragmentByTag(SleepCycleProcessedFragment.TAG) as? SleepCycleProcessedFragment;
            if (mProcessedFragment == null)
                mProcessedFragment = SleepCycleProcessedFragment.newInstance();
        }
    }

    fun setProcessedBundleArgs(savedTime: Boolean = false) {
        val args: Bundle = Bundle();
        if (!savedTime) {
            val selectedTime: SelectedTime = mSetFragment!!;
            val time: Time = selectedTime.getTime();
            args.putSerializable(SleepCycleProcessedFragment.SET_WAKE_UP, time);
        } else
            args.putBoolean(SleepCycleProcessedFragment.SAVED_WAKE_UP, true);
        mProcessedFragment!!.arguments = args;
    }

    private fun addProcessedFragment(savedTime: Boolean = false) {
        initProcessedFragment()
        setProcessedBundleArgs(savedTime)
        supportFragmentManager.beginTransaction()
                .replace(R.id.container, mProcessedFragment, SleepCycleProcessedFragment.TAG)
                .addToBackStack("BACK_STACK")
                .commit();
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
    }
}
