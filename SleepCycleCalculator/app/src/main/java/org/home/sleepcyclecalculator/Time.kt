package org.home.sleepcyclecalculator

import java.io.Serializable

/**
 * hour is hour of the day [0,23]
 */
data class Time(val hour: Int, val minutes: Int) : Serializable {

    private var _wakeUpTimeInMinutes: Int = 0;

    init {
        assert(minutes >= 0 && minutes < 60 && hour >= 0 && hour < 24)
        _wakeUpTimeInMinutes = hour * 60 + minutes;
    }

    val wakeUpTimeInMinutes: Int get() {
        return _wakeUpTimeInMinutes;
    };

    /**
     * this offset is tailored around the fact that you can't take more than 30 minutes offset<br>
     * //TODO: make it general for any minutes amount
     */
    public fun offset(minutes: Int): Time {
        val delta = this.minutes + minutes
        var h = this.hour;
        if (delta < 0) {
            h -= 1
            if (h < 0)
                h += 24
        }
        val m = if (delta < 0) delta + 60 else delta
        return Time(h, m)
    }

    override fun toString(): String {
        var ampmHour: Int = hour;
        var ampm = "AM";
        if (hour >= 12) {
            ampm = "PM"
        }
        if (hour > 12) {
            ampmHour -= 12
        }
        if (hour == 0) {
            ampmHour = 12
        }
        val h = if (ampmHour < 10) "0$ampmHour" else ampmHour
        val m = if (minutes < 10) "0$minutes" else minutes
        return "$h:$m $ampm"
    }
}