package org.home.sleepcyclecalculator

/**
 * Created by criskey on 28/6/2016.
 */
interface SelectedTime {

    fun getTime(): Time;
}