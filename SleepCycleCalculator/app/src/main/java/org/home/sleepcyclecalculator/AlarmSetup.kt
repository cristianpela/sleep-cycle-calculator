package org.home.sleepcyclecalculator

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.util.Log
import java.util.*

class AlarmSetup(context: Context) {

    companion object {
        val ALARM_MARGIN_REQUEST_ID = 1337
        val ALARM_WAKE_UP_REQUEST_ID = 1338
        val ALARM_MESSAGE_KEY = "ALARM_MESSAGE_KEY"
    }

    private val am: AlarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager

    val hasAlarm: Boolean = PendingIntent.getBroadcast(context, ALARM_MARGIN_REQUEST_ID,
            Intent(context, AlarmReceiver::class.java),  PendingIntent.FLAG_NO_CREATE) !=null || PendingIntent.getBroadcast(context, ALARM_WAKE_UP_REQUEST_ID,
            Intent(context, AlarmReceiver::class.java), PendingIntent.FLAG_NO_CREATE) != null

    private val alarmMarginIntent: PendingIntent? = PendingIntent.getBroadcast(context, ALARM_MARGIN_REQUEST_ID,
            Intent(context, AlarmReceiver::class.java).putExtra(ALARM_MESSAGE_KEY, "Prepare to Sleep"),  PendingIntent.FLAG_UPDATE_CURRENT)
    private val alarmWakeIntent: PendingIntent? = PendingIntent.getBroadcast(context, ALARM_WAKE_UP_REQUEST_ID,
            Intent(context, AlarmReceiver::class.java).putExtra(ALARM_MESSAGE_KEY, "Wake Up"), PendingIntent.FLAG_UPDATE_CURRENT)

    fun set(marginTime: Time, wakeUpTime: Time, repeat: Boolean = false) {

        val calMargin = calendar(marginTime)
        val calWake = calendar(wakeUpTime)

        if (repeat) {
            am.setInexactRepeating(AlarmManager.RTC_WAKEUP, calMargin.timeInMillis, AlarmManager.INTERVAL_DAY, alarmMarginIntent)
            am.setInexactRepeating(AlarmManager.RTC_WAKEUP, calWake.timeInMillis, AlarmManager.INTERVAL_DAY, alarmWakeIntent)
        } else {
            am.set(AlarmManager.RTC_WAKEUP, calMargin.timeInMillis, alarmMarginIntent)
            am.set(AlarmManager.RTC_WAKEUP, calWake.timeInMillis, alarmWakeIntent)
        }

    }

    private fun calendar(time: Time): Calendar {
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = System.currentTimeMillis()
        calendar.set(Calendar.HOUR_OF_DAY, time.hour)
        calendar.set(Calendar.MINUTE, time.minutes)
        Log.d("Calendar", Date(calendar.timeInMillis).toString())
        return calendar
    }

    fun cancel() {
        alarmMarginIntent?.cancel()
        am.cancel(alarmMarginIntent)

        alarmWakeIntent?.cancel()
        am.cancel(alarmWakeIntent)
    }

    fun hasAlarm(){

    }

}