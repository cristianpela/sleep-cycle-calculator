package org.home.sleepcyclecalculator

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import java.util.*

class AlarmReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        context.startActivity(Intent(context, TriggeredAlarmActivity::class.java)
                .putExtra(AlarmSetup.ALARM_MESSAGE_KEY, intent.getStringExtra(AlarmSetup.ALARM_MESSAGE_KEY))
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK))
        Log.d("Alarm Sleep ", "TRIGGERED")
    }

}
