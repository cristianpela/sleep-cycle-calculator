package org.home.sleepcyclecalculator;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ViewParent;
import android.widget.TimePicker;

/**
 * Created by criskey on 29/6/2016.
 */

public class ScrollableTimePicker extends TimePicker {


    public ScrollableTimePicker(Context context) {
        super(context);
    }

    public ScrollableTimePicker(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ScrollableTimePicker(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public ScrollableTimePicker(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO) {
            int actionMasked = ev.getActionMasked();
            if (actionMasked == MotionEvent.ACTION_DOWN || actionMasked == MotionEvent.ACTION_UP) {
                ViewParent p = getParent();
                if (p != null)
                    p.requestDisallowInterceptTouchEvent(true);
            }
        }
        //TODO implement this for API < 7
        return false;
    }
}
