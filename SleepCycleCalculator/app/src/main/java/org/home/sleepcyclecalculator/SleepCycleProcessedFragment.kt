package org.home.sleepcyclecalculator

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Build
import android.os.Bundle
import android.provider.AlarmClock
import android.provider.AlarmClock.*
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import android.widget.SeekBar
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_sleep_cycle_processed.*

import kotlinx.android.synthetic.main.fragment_sleep_cycle_set.*;

/**
 * Created by criskey on 27/6/2016.
 */
class SleepCycleProcessedFragment : Fragment(), SeekBar.OnSeekBarChangeListener {

    private lateinit var wakeUpTime: Time;
    private lateinit var selectedTime: Time;
    private lateinit var marginTime: Time;

    private var selectedRadioId: Int = 0;
    private var lightsOffMargin: Int = LIGHTS_OFF_MIN;
    private var alarmDaily: Boolean = false;
    private lateinit var prefs: SharedPreferences;


    companion object {
        fun newInstance(): SleepCycleProcessedFragment {
            return SleepCycleProcessedFragment();
        }

        const val TAG = "SleepCycleProcessedFragment"
        private const val RADIO_ID_KEY = "sca_radio_index"
        private const val HOUR_KEY = "sca_hour"
        private const val MINUTES_KEY = "sca_minutes"
        private const val LIGHTS_OFF_MARGIN_KEY = "sca_margin"
        private const val ALARM_DAILY_KEY = "sca_daily"
        private const val LIGHTS_OFF_MIN: Int = 10

        private const val ALARM_PREPARE_REQUEST_CODE: Int = 1337
        private const val ALARM_WAKE_UP_REQUEST_CODE: Int = 1338

        const val PREFERENCES = "sca_preferences";
        const val IS_SET = "sca_set";

        const val SET_WAKE_UP = "WAKE_UP_TIME"
        const val SAVED_WAKE_UP = "SAVED_WAKE_UP_TIME"

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        prefs = context.getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE);
        initWakeUpTime(savedInstanceState)
    }

    private fun initWakeUpTime(savedInstanceState: Bundle?) {
        if (arguments.getBoolean(SAVED_WAKE_UP)) {// get from prefs
            wakeUpTime = Time(prefs.getInt(HOUR_KEY, -1), prefs.getInt(MINUTES_KEY, -1))
            lightsOffMargin = prefs.getInt(LIGHTS_OFF_MARGIN_KEY, -1)
            alarmDaily = prefs.getBoolean(ALARM_DAILY_KEY, false)
            selectedRadioId = prefs.getInt(RADIO_ID_KEY, -1)
        } else {
            wakeUpTime = arguments?.getSerializable(SET_WAKE_UP) as Time;
            selectedRadioId = savedInstanceState?.getInt(RADIO_ID_KEY) ?: 0
            lightsOffMargin = savedInstanceState?.getInt(LIGHTS_OFF_MARGIN_KEY) ?: LIGHTS_OFF_MIN
            alarmDaily = savedInstanceState?.getBoolean(ALARM_DAILY_KEY, false) ?: false;
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_sleep_cycle_processed, container, false);
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        val chosenWakeUp = context.getString(R.string.text_chosen_wake_up, wakeUpTime.toString())
        textChosenWakeUp.text = chosenWakeUp;
        textLightsOffMargin.text = context.getString(R.string.text_lights_off_margin_minutes, lightsOffMargin)
        seekBar.setOnSeekBarChangeListener(this);
    }

    override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
        lightsOffMargin = progress + LIGHTS_OFF_MIN
        textLightsOffMargin.text = context.getString(R.string.text_lights_off_margin_minutes, lightsOffMargin)
    }

    override fun onStartTrackingTouch(seekBar: SeekBar?) {
    }

    override fun onStopTrackingTouch(seekBar: SeekBar?) {
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        outState?.putInt(RADIO_ID_KEY, selectedRadioId)
        outState?.putInt(LIGHTS_OFF_MARGIN_KEY, lightsOffMargin)
        outState?.putBoolean(ALARM_DAILY_KEY, checkAlarmDaily.isChecked)
        super.onSaveInstanceState(outState)
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        createSleepingTimeButtons();
        seekBar.progress = lightsOffMargin - LIGHTS_OFF_MIN
        checkAlarmDaily.isChecked = alarmDaily
    }

    fun createSleepingTimeButtons() {
        val calc: SleepCycleCalculator = SleepCycleCalculator(wakeUpTime);
        val sleepingTimes = calc.startSleepingTimes;
        for (i in 0..sleepingTimes.size - 1) {
            val cycleRadioButton: RadioButton = RadioButton(context);
            cycleRadioButton.text = sleepingTimes[i].toString();
            cycleRadioButton.tag = sleepingTimes[i];
            cycleRadioButton.id = i
            radioSleepGroup.addView(cycleRadioButton);
        }
        radioSleepGroup.setOnCheckedChangeListener { radioGroup, radioId ->
            val rb = radioGroup.findViewById(radioId);
            selectedTime = rb.tag as Time;
            selectedRadioId = radioId
        }
        radioSleepGroup.check(selectedRadioId)
    }

    fun setSaveTime() {

        marginTime = selectedTime.offset(-lightsOffMargin)

        val alarm = AlarmSetup(context)
        alarm.set(marginTime, wakeUpTime, checkAlarmDaily.isChecked)

        Toast.makeText(context, "Alarms were set as follows: $marginTime to prepare to sleep and $wakeUpTime to wake up", Toast.LENGTH_LONG).show()
        prefs.commitEdit {
            it.putInt(HOUR_KEY, wakeUpTime.hour)
            it.putInt(MINUTES_KEY, wakeUpTime.minutes)
            it.putBoolean(ALARM_DAILY_KEY, checkAlarmDaily.isChecked)
            it.putInt(LIGHTS_OFF_MARGIN_KEY, lightsOffMargin)
            it.putInt(RADIO_ID_KEY, selectedRadioId)
            it.putBoolean(IS_SET, true)
        }

    }


    fun SharedPreferences.commitEdit(cb: (editor: SharedPreferences.Editor) -> Unit) {
        val editor = this.edit();
        cb(editor);
        editor.commit();
    }

}