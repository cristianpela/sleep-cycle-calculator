package org.home.sleepcyclecalculator

import android.content.Context
import android.os.Build
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import kotlinx.android.synthetic.main.fragment_sleep_cycle_set.*;

/**
 * Created by criskey on 27/6/2016.
 */
class SleepCycleSetFragment : Fragment(), SelectedTime {

    companion object {
        fun newInstance(): SleepCycleSetFragment {
            return SleepCycleSetFragment();
        }

        const val TAG = "SleepCycleSetFragment"
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_sleep_cycle_set, container, false);
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super<Fragment>.onViewCreated(view, savedInstanceState);
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        buttonSaved.visibility = if (!hasPreferencesSaved()) View.GONE else View.VISIBLE
        buttonCancel.visibility = if (!AlarmSetup(context).hasAlarm) View.GONE else View.VISIBLE
    }

    override fun getTime(): Time {
        val hour = if (Build.VERSION.SDK_INT < 23) timePicker.currentHour else timePicker.hour;
        val minute = if (Build.VERSION.SDK_INT < 23) timePicker.currentMinute else timePicker.minute;
        return Time(hour, minute);
    }

    private fun hasPreferencesSaved(): Boolean {
        return context.getSharedPreferences(SleepCycleProcessedFragment.PREFERENCES, Context.MODE_PRIVATE)
                .getBoolean(SleepCycleProcessedFragment.IS_SET, false);
    }

}