package org.home.sleepcyclecalculator

import android.media.MediaPlayer
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.Settings
import android.view.View
import android.view.Window

import kotlinx.android.synthetic.main.activity_triggered_alarm.*

class TriggeredAlarmActivity : AppCompatActivity() {

    val mediaPlayer: MediaPlayer by lazy {
        MediaPlayer.create(applicationContext, Settings.System.DEFAULT_ALARM_ALERT_URI)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_triggered_alarm)
        textAlarm.text = intent.getStringExtra(AlarmSetup.ALARM_MESSAGE_KEY)
        playAlarmSound()
    }

    fun actionStopAlarm(button: View) {
        finish()
    }

    fun playAlarmSound() {
        mediaPlayer.start()
        mediaPlayer.isLooping = true
    }

    override fun onDestroy() {
        mediaPlayer.stop()
        super.onDestroy()
    }
}
