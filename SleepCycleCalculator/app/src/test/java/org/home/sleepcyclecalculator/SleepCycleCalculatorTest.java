package org.home.sleepcyclecalculator;

import junit.framework.TestCase;

import org.junit.Test;

import java.util.Collections;

import static org.junit.Assert.*;

/**
 * Created by criskey on 27/6/2016.
 */
public class SleepCycleCalculatorTest extends TestCase {

    @Test
    public void testCalculator() {
        SleepCycleCalculator calc = new SleepCycleCalculator(new Time(6, 30));
        assertArrayEquals(new Time[]{
                new Time(21, 30),
                new Time(23, 00),
                new Time(00, 30),
                new Time(02, 00)
        }, calc.getStartSleepingTimes());


        calc = new SleepCycleCalculator(new Time(22, 00));
        assertArrayEquals(new Time[]{
                new Time(13, 00),
                new Time(14, 30),
                new Time(16, 00),
                new Time(17, 30)
        }, calc.getStartSleepingTimes());


        calc = new SleepCycleCalculator(new Time(00, 00));
        assertArrayEquals(new Time[]{
                new Time(15, 00),
                new Time(16, 30),
                new Time(18, 00),
                new Time(19, 30)
        }, calc.getStartSleepingTimes());
    }

    @Test
    public void testTimeOffset() {
        assertEquals(new Time(0, 0), new Time(0, 15).offset(-15));
        assertEquals(new Time(23, 55), new Time(0, 15).offset(-20));
        assertEquals(new Time(0, 35), new Time(0, 15).offset(20));
    }
}